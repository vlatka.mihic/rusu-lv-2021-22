from sklearn.preprocessing import PolynomialFeatures

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import confusion_matrix

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


np.random.seed(242)
data = generate_data(200)

poly = PolynomialFeatures(degree=3, include_bias = False)
data_new = poly.fit_transform(data[:,0:2])

x1 = data[:,0:1]
x2 = data[:,2:8]
X = np.stack((x1, x2), axis=1)
klase = data[:,2]


np.random.seed(12)
train = generate_data(100)
t1 = train[:,0]
t2 = train[:,1]
T = np.stack((t1, t2), axis=1)
klaseT = train[:,2]

poly = PolynomialFeatures(degree=3, include_bias = False)
train_new = poly.fit_transform(train[:,0:2])

reg = LinearRegression().fit(T, klaseT)
theta1 = reg.coef_
theta0 = reg.intercept_


klaseP = reg.predict(X)

for i in range(0, len(klaseP)):
    if(klaseP[i] >= 0.5):
        klaseP[i] = 1
    else:
        klaseP[i] = 0        

TPTN = []

for i in range(0, len(klaseP)):
    if(klaseP[i] == klase[i]):
        TPTN.append('#51f542')
    else:
        TPTN.append('#000000')

plt.figure(1)
plt.scatter(x1, x2, c=TPTN)

c_mx = confusion_matrix(klase, klaseP)

plot_confusion_matrix(c_mx)
