import numpy as np
from numpy import random
import matplotlib.pyplot as plt

n = int(input('Veličina vektora: '))

osobe = np.random.randint(2, size=n)
visine = osobe
zene = []
muskarci = []

for visina in visine:
    if(visina == 1):
        visina = round(float(random.normal(loc=180, scale=7, size=1)))
        zene.append(visina)
    elif(visina == 0):
        visina = round(float(random.normal(loc=167, scale=7, size=1)))
        muskarci.append(visina)
    print(visina)

plt.hist([zene, muskarci])
plt.legend(["Žene", "Muškarci"], loc ="upper right")
plt.show()