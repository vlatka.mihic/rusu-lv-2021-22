import csv
import matplotlib.pyplot as plt
import numpy as np

file = open("/Users/vlatkamihic/Desktop/FX/rusu-lv-2021-22/LV2/resources/mtcars.csv", "r")
csv_reader = csv.reader(file)

lists_from_csv = []
for row in csv_reader:
    lists_from_csv.append(row)


lista_automobila = []


for row in lists_from_csv:
    row_auto = []
    row_auto.append(row[0])
    row_auto.append(row[1])
    row_auto.append(row[4])
    row_auto.append(row[6])
    lista_automobila.append(row_auto)


lista_automobila.pop(0)
mpg = []
hp = []
wt = []

for row in lista_automobila:
    mpg.append(row[1])
    hp.append(row[2])
    wt.append(row[3])
    

# Ispišite minimalne, maksimalne i srednje vrijednosti potrošnje automobila

print('\nMinimalna potrošnja automobila: ' + min(mpg) + ' mpg')
print('Maksimalna potrošnja automobila: ' + max(mpg) + ' mpg')

avg = sum([float(item) for item in mpg])/len(mpg)
print('Srednja vrijednost potrošnje automobila: ', str(round(avg, 2)), ' mpg')

potrosnja_automobila = np.array(mpg)
konjska_snaga = np.array(hp)
tezina = np.array(wt)


ovisnost = list(zip(potrosnja_automobila, konjska_snaga, tezina))

new_x, new_y, new_z = zip(*sorted(ovisnost))

new_mpg = np.array(new_x)
new_hp = np.array(new_y)
new_wt = np.array(new_z)

plt.figure(figsize=(20,15))
plt.plot(new_mpg, new_hp, marker = 'o')

plt.title("Ovisnost potrošnje automobila (mpg) o konjskim snagama (hp)")
plt.xlabel("Miles per gallon")
plt.ylabel("Horsepower")
plt.grid()

for i, txt in enumerate(new_wt):
    plt.annotate(str(txt)+' t', (new_mpg[i], new_hp[i]))
plt.show()