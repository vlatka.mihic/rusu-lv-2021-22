import numpy as np
from numpy import random
import matplotlib.pyplot as plt

bacanja_kockice = np.random.choice(range(1,7), 100)

# print(bacanja_kockice)

plt.hist([bacanja_kockice])
plt.legend(["Bacanja kockice"], loc ="upper right")
plt.show()