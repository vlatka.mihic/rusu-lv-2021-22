
import re

naziv_datoteke = input("Unesite naziv datoteke za citanje: ")

datoteka = open(naziv_datoteke, 'r')

adrese = re.findall(r'[\w\.-]+@[\w\.-]+', datoteka.read())

regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
adresee = []

for adresa in adrese:
    if(re.fullmatch(regex, adresa)):
            adresee.append(adresa)

# for adresa in adresee:
#     print(adresa)

unique = list(dict.fromkeys(adresee))

izdvojene1  = []
izdvojene2  = []
izdvojene3  = []
izdvojene4  = []
izdvojene5  = []


for adresa in  unique:
    if(re.fullmatch(r'.*a+.*', adresa)):
        izdvojene1.append(adresa)
    if(re.fullmatch(r'.*a.*', adresa)):
        izdvojene2.append(adresa)
    if(not re.fullmatch(r'.*a+.*', adresa)):
        izdvojene3.append(adresa)
    if(re.fullmatch(r'.*[0-9].*', adresa)):
        izdvojene4.append(adresa)
    if(re.fullmatch(r'^[a-z]+@[a-z.]+$', adresa)):
        izdvojene5.append(adresa)

print('\n')
print('sadrži najmanje jedno slovo a \n')
for adresa in izdvojene1:
    print(adresa)
    
print('\n')
print('sadrži točno jedno slovo a \n')
for adresa in izdvojene2:
    print(adresa)
    
    
print('\n')
print('ne sadrži slovo a \n')
for adresa in izdvojene3:
    print(adresa)
    
    
print('\n')
print('sadrži jedan ili više numeričkih znakova (0 – 9) \n')
for adresa in izdvojene4:
    print(adresa)
    
    
print('\n')
print('sadrži samo mala slova (a-z) \n')
for adresa in izdvojene5:
    print(adresa)
