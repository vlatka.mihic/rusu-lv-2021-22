import re

naziv_datoteke = input("Unesite naziv datoteke za citanje: ")

datoteka = open(naziv_datoteke, 'r')

adrese = re.findall(r'[\w\.-]+@[\w\.-]+', datoteka.read())

regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
adresee = []

for adresa in adrese:
    if(re.fullmatch(regex, adresa)):
            adresee.append(adresa)

# for adresa in adresee:
#     print(adresa)

unique = list(dict.fromkeys(adresee))

for adresa in unique:
    print(adresa)