import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import PIL

def truncate(x):
    '''makes sure returned value is between 0 and 255'''
    return min(255, max(0, x))


image = PIL.Image.open('/Users/vlatkamihic/Desktop/FX/rusu-lv-2021-22/LV2/resources/tiger.png')
np_array = np.array(image)
new_img = np.array(np.ones(np_array.shape))

print(np_array[0][0])

#Parametar kojim mijenjamo svjetlinu slike
deltaBrighten = 50

for i in range(0, np_array.shape[0]):
    for j in range(0, np_array.shape[1]):
        new_img[i][j] = (new_img[i][j])*(np_array[i][j]) + deltaBrighten
        new_img[i][j] = truncate(new_img[i][j])
    
    
brighen_img = new_img.astype(np.uint8())

img = PIL.Image.fromarray(brighen_img, 'P')
img.show()