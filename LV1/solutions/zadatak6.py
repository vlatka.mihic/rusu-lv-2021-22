
naziv_datoteke = input("Unesite naziv datoteke za citanje: ")

datoteka = open(naziv_datoteke)

adrese = []

for redak in datoteka:
    redak = redak.rstrip()

    if redak.startswith('From: '):
        redak = redak.replace('From: ', '')
        temp = redak.split(' ')
        temp2 = temp[0].split('@')
        adrese.append(temp2[1])

unique = list(dict.fromkeys(adrese))

dictionary = dict()

for hostname in unique:
    i = 0
    for adresa in adrese:
        if(hostname == adresa):
            i = i+1
    dictionary[hostname] = i

for k, v in dictionary.items():
    print("Key: " + str(k) + "; Value: " + str(v))

