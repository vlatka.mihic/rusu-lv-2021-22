
naziv_datoteke = input("Unesite naziv datoteke za citanje: ")

datoteka = open(naziv_datoteke)

sum = 0
n = 0

for redak in datoteka:
    redak = redak.rstrip()

    if redak.startswith('X-DSPAM-Confidence:'):
        n = n + 1
        redak = redak.replace('X-DSPAM-Confidence: ', '')
        redak = float(redak)
        sum = sum + redak

average = float(sum/n)

print("Average X-DSPAM-Confidence: " + str(average))