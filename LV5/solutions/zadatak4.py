# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 17:30:46 2021

@author: student
"""

import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt



face = mpimg.imread('example_grayscale.png') 

# try:
#     face = sp.face(gray=True)
# except AttributeError:
#     from scipy import misc
#     face = misc.face(gray=True)
    
X = face.reshape((-1, 1)) # We need an (n_sample, n_feature) array
 
j = 1
for i in range(2, 7):  
    k_means = cluster.KMeans(n_clusters=i,n_init=1)
    k_means.fit(X) 
    values = k_means.cluster_centers_.squeeze()
    labels = k_means.labels_
    face_compressed = np.choose(labels, values)
    face_compressed.shape = face.shape
    
    plt.figure(j)
    plt.imshow(face,  cmap='gray')
    
    j = j+1
    plt.figure(j)
    plt.imshow(face_compressed,  cmap='gray')
    j = j+1
