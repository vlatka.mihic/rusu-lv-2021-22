from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from random import randint

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          centers=4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(500, 4)

plt.figure(1)
plt.scatter(data[:,0], data[:,1])

n_clusters = 3

kmeans = KMeans(n_clusters, random_state=0).fit(data)
centers = kmeans.cluster_centers_


plt.scatter(centers[:,0], centers[:,1], marker = 'o')

y_pred = kmeans.predict(data)

colors = []
color = []
for j in range(0, n_clusters):
    color.append('#{:06x}'.format(randint(0, 256**3)))

for i in range(0, data.shape[0]):
    for j in range(0, n_clusters):
        if(y_pred[i] == j):
            colors.append(color[j])

plt.scatter(data[:,0], data[:,1], c=colors, marker="o", picker=True)
plt.scatter(centers[:,0], centers[:,1], marker = 'o', c='#FF0000')
plt.show()
    


