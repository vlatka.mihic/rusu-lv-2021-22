import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

# za svaki ulazni podatak ulaznog vektora racunamo izlaznu veličinu po nekoj formuli, y = f(x)
# (4-3)
def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

# dodajemo pogrešku (4-3)
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy


# Ulazi vektor od n(100) ulaznih podataka (4-1)
x = np.linspace(1,10,100)

# Stvarni izlaz 
y_true = non_func(x)

# Izmjereni izlaz - s odstupanjem
y_measured = add_noise(y_true)

# prikaz stvarnih i izmjerenih vrijednosti o ovisnosti o ulazu na grafu
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)

# generiranje n(100) random pozitivnih cijelih brojeva vrijednosti od 0 do 100 
# indeksi ulaznog vektora
indeksi = np.random.permutation(len(x))

# uzimamo 70% podataka za treniranje
# floor(k) vraća najveći cijeli broj manji od k
# u polje indeksi_train spremamo prvih 70% indeksa
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]

# u polje indeksi_train spremamo ostalih 30% indeksa
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

# od polja x i y_measured stvaramo vektor stupce
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

# u vektor x_train i y_train spremamo podatke iz x i y na indeksima indeksi_train
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

# u vektor x_test i y_test spremamo podatke iz x i y na indeksima indeksi_test
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

# prikaz testnih i treniranih podataka na grafu
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

# (4-7)
linearModel = lm.LinearRegression()

# (4-5)
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
# (4-8)
# intercept_ -> theta0
# coef_ -> theta1...thetam
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

# (4-4)
ytest_p = linearModel.predict(xtest)

# (4-14)
MSE_test = mean_squared_error(ytest, ytest_p)
print('MSE = ', MSE_test)

# prikaz testnih podataka koji su predviđeni(zeleno) i onih koji nisu predviđeni(crveno)
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
# prikaz modela h
plt.plot(x_pravac, y_pravac)
