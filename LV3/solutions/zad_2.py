# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 13:41:28 2021

@author: student
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def carWith8CylData(mtcars):
    
    car8cylDataFrame = mtcars[mtcars.cyl == 8]
    
    return car8cylDataFrame
    
def carWith6CylMpgData(mtcars):
    
    car6cylDataFrame = mtcars[mtcars.cyl == 6]
    
    return car6cylDataFrame
    

def carWith4CylMpgData(mtcars):
    
    car4cylDataFrame = mtcars[mtcars.cyl == 4]
    
    return car4cylDataFrame

##################################################################

def carWith8Cyl(mtcars):
    
    car8cylDataFrame = mtcars[mtcars.cyl == 8]
    car8cyl = car8cylDataFrame.index.to_numpy()
    car8cylMpg = car8cylDataFrame['mpg'].to_numpy()
    
    return (car8cyl, car8cylMpg)
    
def carWith6CylMpg(mtcars):
    
    car6cylDataFrame = mtcars[mtcars.cyl == 6]
    car6cyl = car6cylDataFrame.index.to_numpy()
    car6cylMpg = car6cylDataFrame['mpg'].to_numpy()
    
    return (car6cyl, car6cylMpg)
    

def carWith4CylMpg(mtcars):
    
    car4cylDataFrame = mtcars[mtcars.cyl == 4]
    car4cyl = car4cylDataFrame.index.to_numpy()
    car4cylMpg = car4cylDataFrame['mpg'].to_numpy()
    
    return (car4cyl, car4cylMpg)

##################################################################

def plotMpgFor468Cyl(mtcars):
    
    (car8, mpg8) = carWith8Cyl(mtcars)

    (car6, mpg6) = carWith6CylMpg(mtcars)
    
    (car4, mpg4) = carWith4CylMpg(mtcars)
    
    barWidth = 0.25
    fig = plt.subplots(figsize =(12, 8))
    plt.bar(car4, mpg4, color ='r', width = barWidth,
            edgecolor ='grey', label ='Cars with 4 cylinders')
    plt.bar(car6, mpg6, color ='g', width = barWidth,
            edgecolor ='grey', label ='Cars with 6 cylinders')
    plt.bar(car8, mpg8, color ='b', width = barWidth,
            edgecolor ='grey', label ='Cars with 8 cylinders')
    
    plt.legend()


def plotWtFor468Cyl(mtcars):
    car4 = carWith4CylMpgData(mtcars).wt
    car6 = carWith6CylMpgData(mtcars).wt
    car8 = carWith8CylData(mtcars).wt
    
    
    car4df = pd.DataFrame(car4.to_numpy(), columns=['4'])
    car6df = pd.DataFrame(car6.to_numpy(), columns=['6'])
    car8df = pd.DataFrame(car8.to_numpy(), columns=['8'])
    
    allFrames = pd.concat([car4df, car6df, car8df])
    
    plt.figure()
    allFrames.boxplot()
    

def carAMTrueFalse(mtcars):
    
    carAmTrue = mtcars[mtcars.am == 1].iloc[:, 9]
    carAmFalse = mtcars[mtcars.am == 0].iloc[:, 9]
    print("Broj automobila sa automatskim mjenjačem: ", carAmTrue.count())
    print("\nBroj automobila s ručnim mjenjačem: ", carAmFalse.count())
    print("\n\n")
    

################################################################

mtcars = pd.read_csv('mtcars.csv')
# plotMpgFor468Cyl(mtcars)
plotWtFor468Cyl(mtcars)




