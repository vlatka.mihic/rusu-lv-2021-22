# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 12:20:29 2021

@author: student
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



########################## 1. ################################

def carMpgSort(mtcars):
    mpg = mtcars.iloc[:, 0:2]

    # print(mpg) 
    
    mpgSort = mpg.sort_values(by ='mpg', ascending=False)
    # print(mpgSort)
    
    
    mpgMax = mpgSort.iloc[0:6, :]
    
    print("\nNajveću potrošnju imaju sljedeći automobili: \n")
    print(mpgMax)
    print("\n\n")




########################## 2. ################################

def carWith8Cyl(mtcars):
    
    car8cyl = mtcars[mtcars.cyl == 8].iloc[:, 0:2]
    # print(car8cyl)
    
    car8cylMpgSort = car8cyl.sort_values(by = 'mpg', ascending=False)
    
    print("Tri automobila s 8 cilindara koja imaju najveću potrošnju su: \n")
    print(car8cylMpgSort.iloc[0:3, :])
    print("\n\n")


########################## 3. ################################

def carWith6Cyl(mtcars):
    
    car6cyl = mtcars[mtcars.cyl == 6].iloc[:, 0:2]
    # print(car6cyl)
    
    new_car6cyl = car6cyl.iloc[:, 1]
    
    print("Srednja potrošnja automobila sa 6 cilindara je: \n")
    print(new_car6cyl.mean(), 'mpg')
    print("\n\n")

########################## 4. ################################

def carWith4Cyl(mtcars):
    
    car4cyl = mtcars[(mtcars.cyl == 4) & (mtcars.wt > 2.0) & (mtcars.wt <= 2.2)].iloc[:, [0, 1, 6]]
    # print(car4cyl)
    
    new_car4cyl = car4cyl.iloc[:, 1]
    
    print("Srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs je: \n")
    print(new_car4cyl.mean(), ' mpg')
    print("\n\n")


########################## 5. ################################

def carAMTrueFalse(mtcars):
    
    carAmTrue = mtcars[mtcars.am == 1].iloc[:, 9]
    carAmFalse = mtcars[mtcars.am == 0].iloc[:, 9]
    print("Broj automobila sa automatskim mjenjačem: ", carAmTrue.count())
    print("\nBroj automobila s ručnim mjenjačem: ", carAmFalse.count())
    print("\n\n")


########################## 6. ################################

def carHPOver100(mtcars):
    
    carHPOver100 = mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].iloc[:, 9]
    print("\nBroj automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga: ", carHPOver100.count())
    print("\n\n")


########################## 7. ################################

def carWeightLbsToKg(mtcars):
    
    carWeightsKg = mtcars.iloc[:, [0, 6]]
    # print(carWeightsKg)
    # print("\n\n")
    
    carWeightsKg.wt = carWeightsKg.wt.multiply(1000*0.45359237)
    print("\nMasa svakog automobila u kilogramima iznosi: \n\n", carWeightsKg)
    print("\n\n")


mtcars = pd.read_csv('mtcars.csv')
# print(len(mtcars))
print(mtcars.iloc[:,:]) 

carMpgSort(mtcars)

carWith8Cyl(mtcars)

carWith6Cyl(mtcars)

carWith4Cyl(mtcars)

carAMTrueFalse(mtcars)

carHPOver100(mtcars)

carWeightLbsToKg(mtcars)
