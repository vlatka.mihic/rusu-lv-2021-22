#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 23:27:44 2021

@author: vlatkamihic
"""

import urllib.request as urll
import pandas as pd
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

# FUNCTION TO REMOVE TIMEZONE
def remove_timezone(dt):
   
    # HERE `dt` is a python datetime
    # object that used .replace() method
    return dt.replace(tzinfo=None)


# Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek.

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = urll.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1
  

df.vrijeme = pd.to_datetime(df.vrijeme)
df['vrijeme'] = df['vrijeme'].apply(remove_timezone)

df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df.vrijeme.dt.month
df['dayOfweek'] = df.vrijeme.dt.dayofweek



# Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.

df['date'] = df.vrijeme.dt.date
df.to_csv('out.csv', index=False) 
dfSorted = df.sort_values('mjerenje')

print("Koncentracija PM10 bila je najveća: ")
prvaTri = dfSorted.iloc[0:3, 4].to_numpy()
for date in prvaTri:
    print(date.strftime('%b %d,%Y'))
    


# Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.

deltas = df['date'].diff()[1:]
mentioned = df.groupby(df['month']).size().to_numpy()
expected = [31,28,31,30,31,30,31,31,30,31,30,31]

missing = expected - mentioned
months = ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"]


barWidth = 0.5
plt.figure()
plt.xticks(
    rotation=45, 
    horizontalalignment='right',
    fontweight='light',
    fontsize='x-large'  
)
plt.grid(zorder=0)
plt.bar(months, missing, color ='b', width = barWidth,edgecolor ='grey', zorder=3)
plt.title("Ukupan broj izostalih vrijednosti tijekom svakog mjeseca")

# Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.

january = df[df.month == 1].mjerenje.to_numpy()
july = df[df.month == 7].mjerenje.to_numpy()

n = min(len(january), len(july))
compare = pd.DataFrame()

for i in range(0, n):
    row = dict(zip(['January', 'July'], [january[i], july[i]]))
    row_s = pd.Series(row)
    row_s.name = i
    compare = compare.append(row_s)
    

print(compare)

plt.figure()
plt.title("Usporedba PM10 koncentracije tijekom jednog zimskog i jednog ljetnog mjeseca")
compare.boxplot()

# Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda. 

df['weekend'] = df['dayOfweek'].replace([0, 1, 2, 3, 4, 5, 6], [False, False, False,False, False, True,True])
weekDay = df[df.weekend == False].mjerenje.to_numpy()
weekEnd = df[df.weekend == True].mjerenje.to_numpy()

plt.figure()
plt.title("Usporedba distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda")
plt.hist(weekDay, label ='Weekdays')
plt.hist(weekEnd, label ='Weekends')
plt.xlabel('koncentracija PM10 čestca')
plt.legend()
plt.show()


  
